/*** Network Presence Z-Way HA module ****************************************
Version: 0.5
------------------------------------------------------------------------------
Author: Kjetil Volden
Description:
    This module is used for pinging one or more network devices and determine
    their presence.
******************************************************************************/

// ----------------------------------------------------------------------------
// --- Class definition, inheritance and setup
// ----------------------------------------------------------------------------

function NetworkPresence(id, controller){
    // Call superconstructor first (AutomationModule)
    NetworkPresence.super_.call(this, id, controller);
    this.icon_path  = '/ZAutomation/api/v1/load/modulemedia/NetworkPresence/';
    this.vdevs = [];
    this.default_advanced_config = {method: 'Netcat',
                                    interval: 1,
                                    netcat_timeout: 10,
                                    ping_timeout: 2,
                                    netcat_port: 80,
                                    ping_count: 1,
                                    hysteresis: false,
                                    hysteresis_size: 3,
                                    debug_logging: false};
    this.crontask_name = undefined;
    var self = this;
    this.poll_callback = function(){
        self.poll_all_devices(self);
    }
    this.langfile = this.controller.loadModuleLang("NetworkPresence");
}

inherits(NetworkPresence, AutomationModule);

_module = NetworkPresence;


// ----------------------------------------------------------------------------
// --- Module instance initialized
// ----------------------------------------------------------------------------

NetworkPresence.prototype.init = function (config){
    config.advanced_config = _.extend({}, this.default_advanced_config, config.advanced_config);

    NetworkPresence.super_.prototype.init.call(this, config);

    var self = this;
    this.debug_log = _.bind(this.debug_log, self);

    var crontime = {minute: [0, 59, config.advanced_config.interval],
                    hour: null,
                    weekDay: null,
                    day: null,
                    month: null};

    this.crontask_name = 'networkPresence_' + self.id + '.poll';
    this.controller.emit('cron.addTask', this.crontask_name, crontime);
    this.controller.on(this.crontask_name, self.poll_callback);

    config.network_devices.forEach(function (network_device, index){
        var dev_id = 'NetworkPresence_' + index + '_' + self.id;
        var dev_title = 'Network Presence ' + ((network_device.name !== "") ? network_device.name : network_device.addr);
        self.vdevs.push(this.controller.devices.create({
            deviceId: dev_id,
            defaults: {
                deviceType: 'sensorBinary',
                metrics: {
                    level: 'off',
                    icon: self.icon_path + 'icon.png',
                    title: dev_title
                }
            },
            overlay: {
                metrics: {
                    addr: network_device.addr,
                    responses: [],
                    last_seen: undefined
                }
            },
            handler: function (command){
                if(command === 'update'){
                    var this_dev = self.controller.devices.get(dev_id);
                    self.poll_device(this_dev, self);
                }
            },
            moduleId: self.id
        }));
    });

    // Update created devices
    self.poll_all_devices(self);
};


// ----------------------------------------------------------------------------
// --- Module instance stopped
// ----------------------------------------------------------------------------

NetworkPresence.prototype.stop = function (){
    NetworkPresence.super_.prototype.stop.call(this);
    var self = this;
    this.vdevs.forEach(function (device){
        self.controller.devices.remove(device.id);
    });
    this.vdevs = [];
    this.controller.emit('cron.removeTask', this.crontask_name);
    this.controller.off(this.crontask_name, self.poll_callback);
};


// ----------------------------------------------------------------------------
// --- Poll all configured addresses
// ----------------------------------------------------------------------------

NetworkPresence.prototype.poll_all_devices = function (self){
    self.debug_log("Polling all devices")
    self.vdevs.forEach(function (device){
        _.defer(self.poll_device, device, self);
    });
};


// ----------------------------------------------------------------------------
// --- Poll address of a single vDev
// ----------------------------------------------------------------------------

NetworkPresence.prototype.poll_device = function (device, self){
    var addr = device.get('metrics:addr');
    var method = self.config.advanced_config.method;
    self.debug_log("Polling device " + device.deviceId + " using method " + method);

    try {
        if(method === 'Ping'){
            var c = self.config.advanced_config.ping_count;
            var w = self.config.advanced_config.ping_timeout;
            var response = system('ping -c ' + c + ' -w ' + w + ' ' + addr);
            // PING has exit code 0 when response received:
            var alive = response[0] == 0;
        }
        else if(method === 'Netcat'){
            var w = self.config.advanced_config.netcat_timeout;
            var p = self.config.advanced_config.netcat_port;
            var response = system('netcat -v -z -w ' + w + ' ' + addr + ' ' + p + ' 2>&1');
            var alive = response[1].indexOf("No route to host") == -1;
        }
    }
    catch(err){
        var msg = method === "Ping" ? self.langfile.error_syscommand_ping : self.langfile.error_syscommand_netcat;
        self.controller.addNotification(
            "error",
            msg,
            "module",
            "NetworkPresence"
        );
        self.debug_log("Polling failed, check syscommands")
        return;
    }

    self.debug_log("Command output:\n" + response[1]);
    self.debug_log("Device found to be " + (alive ? "present" : "absent"));
    var device_history = device.get('metrics:responses');
    var history_length = self.config.advanced_config.hysteresis ? self.config.advanced_config.hysteresis_size : 1;
    device_history.push(alive);
    device_history = device_history.slice(-history_length);

    var present = device_history.indexOf(true) !== -1;
    var new_state = present ? 'on' : 'off';
    if(new_state != device.get('metrics:level')){
        device.set('metrics:level', new_state);
    }
    if(alive){
        device.set('metrics:last_seen', Date.now());
    }
    device.set('metrics:responses', device_history);
};

// ----------------------------------------------------------------------------
// --- Logging for debugging purposes
// ----------------------------------------------------------------------------

NetworkPresence.prototype.debug_log = function (msg){
    if(this.config.advanced_config.debug_logging){
        console.log("[NetworkPresence] " + msg);
    }
};
