# Z-Way Network Presence

Network Presence is a home automation module for the Z-Way home automation system, enabling the user to track the presence of devices connected to the network, determined by periodically polling for their presence. Each added IP or host will be represented by a binary sensor virtual device.

NOTE 1: For this module to work, the user has to allow calling the system commands "netcat" and/or "ping" by adding "netcat" and/or "ping" to the file /opt/z-way-server/automation/.syscommands. Netcat is used by default, but Ping can be chosen under Advanced configuration.

NOTE 2: Many modern smart phones put their WiFi in sleep mode after the display is turned off, and in that state they will normally not reply to ping. It is, however, usually possible to determine their presence by attempting a socket connection; if the socket connection is accepted, refused or times out (provided a high enough timeout value) it is normally a sign that the device is present, while if the socket cannot be routed it is normally absent.

# Configuration

## Show advanced configuration

Check this to show the advanced configuration.

### Method

The method for polling. Either Netcat or Ping.

### Interval

The number of minutes to wait between polling devices.

### Netcat timeout

The number of seconds to wait for socket connection. Must be long enough to allow Netcat to determine device absence without timing out, but should be shorter than Interval time (note different units).

### Ping timeout

The number of seconds for which ping will wait for a response.

### Netcat port

The port to which Netcat will attempt to connect. The port does not have to be open on the device.

### Ping count

The number of packets ping sends.

### Hysteresis

If enabled, devices will be considered online if present in any of a number of previous polling attempts. Otherwise they will be judged on latest attempt only.

### Hysteresis size

The number of polling rounds to consider in hysteresis.

### Extra logging

Produce more log output, mostly for debugging purposes.

## Network devices

The devices potentially connected to the network that the module will attempt to poll.

### IP or host

The address of the device. Usually an IP, but potentially any resolvable hostname/address that the netcat or ping command on the Z-Way device will recognize.

### Name

Any string that identifies the device. Could be anything, and is just used to name the virtual device that will be created to represent the network device in Z-Way.

# Installation

The preferred way to install this module is through the "Z-Wave.me App Store."

# License

The icon is a modified version of Lorc's Radar Sweep Icon, licensed under CC BY 3.0 (http://game-icons.net/lorc/originals/radar-sweep.html).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
